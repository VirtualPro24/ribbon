const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const concat = require('gulp-concat');

const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();

//Порядок подключения css файлов
const scssFiles = [
   './src/include/normalize.css', 
   './src/scss/header.scss', 
   './src/scss/main.scss',
   './src/scss/categories.scss',  
    
]
//Порядок подключения js файлов
const jsFiles = [
   './src/js/jquery.min.js',      
   './src/js/lazyload.js',      
   './src/js/wow.min.js',      
   './src/js/scripts.js'     
]


//Таск на скрипты JS
function scripts() {
   //Шаблон для поиска файлов JS
   //Всей файлы по шаблону './src/js/**/*.js'
   return gulp.src(jsFiles)
   //Объединение файлов в один
   .pipe(concat('scripts.js'))
   //Минификация JS
   .pipe(uglify({
      toplevel: true
   }))
   //Выходная папка для скриптов
   .pipe(gulp.dest('./build/js'))
   .pipe(browserSync.stream());
}

function styles() {
	return gulp.src(scssFiles)
	.pipe(concat('style.scss'))
	.pipe(sourcemaps.init())
	.pipe(sass().on('error', sass.logError))
	.pipe(sourcemaps.write('./'))
	.pipe(cleanCSS({
	  level: 2
	}))	
	.pipe(gulp.dest('./build/css/'))
	.pipe(browserSync.stream());
}

function clean() {
   return del(['build/*'])
}

//Таск вызывающий функцию styles
gulp.task('styles', styles);
//Таск вызывающий функцию scripts
gulp.task('scripts', scripts);
//Таск для очистки папки build
gulp.task('del', clean);

gulp.task('watch', function(){
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});	
	gulp.watch('./src/scss/**/*.scss', styles)
	gulp.watch('./src/js/**/*.js', scripts)	
	//При изменении HTML запустить синхронизацию
	gulp.watch("./*.html").on('change', browserSync.reload);
})

gulp.task('build', gulp.series(clean, gulp.parallel(styles,scripts)));
gulp.task('dev', gulp.series('build','watch'));

 
