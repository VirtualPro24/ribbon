function navToggle() {
    $('.toggle-menu').click(function (e) {
        e.preventDefault();

        $(this).toggleClass('active');
        $('.navbar').toggleClass('active');
    });
}

$(document).ready(function () {
    navToggle();

    setTimeout(function () {
        $('.ribbon-path').addClass('fill');
    }, 3000);
    
    if ($(window).width() >= 680) {
        new WOW().init();
    }

    //    $(".lazyload").lazyload();
});

$(window).resize(function () {

});
